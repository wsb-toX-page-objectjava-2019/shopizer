package wsb;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageObject {

    protected WebDriver driver;

    public PageObject(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "img.logoImage")
    public WebElement mainPageLogo;

    @FindBy(id = "customerAccount")
    public WebElement myAccountButton;

    @FindBy(id = "customerAccount")
    public WebElement signInButton;

    @FindBy(id = "registerLink")
    public WebElement registerButton;

    public void clickMyAccount() {
        waitUntilElementIsClickable(myAccountButton);
        myAccountButton.click();
        waitUntilElementIsVisible(registerButton);
    }

    public void clickRegisterButton(){
        waitUntilElementIsClickable(registerButton);
        registerButton.click();
    }

    public void waitUntilElementIsClickable(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void waitUntilElementIsVisible(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOf(element));
    }
}
