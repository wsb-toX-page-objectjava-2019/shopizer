package wsb;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import static junit.framework.TestCase.assertEquals;

public class RegistrationPage extends PageObject{

    public RegistrationPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "firstName")
    public WebElement firstNameInputField;

    @FindBy(id = "lastName")
    public WebElement lastNameInputField;

    @FindBy(id = "registration_country")
    public WebElement countryDropdown;

    @FindBy(id = "hidden_zones")
    public WebElement stateInputField;

    @FindBy (id = "emailAddress")
    public WebElement emailInputField;

    @FindBy (id = "password")
    public WebElement passwordInputField;

    @FindBy (id = "passwordAgain")
    public WebElement repeatPasswordInputField;

    @FindBy (xpath = "//form/button")
    public WebElement createAccountButton;

    public void provideFirstName(String firstName) {
        waitUntilElementIsClickable(firstNameInputField);
        firstNameInputField.clear();
        firstNameInputField.sendKeys(firstName);
    }

    public void provideLastName(String lastName) {
        waitUntilElementIsClickable(lastNameInputField);
        lastNameInputField.clear();
        lastNameInputField.sendKeys(lastName);
    }

    public void chooseCountry (String country) {
        waitUntilElementIsClickable(countryDropdown);
        Select countrySelect = new Select(countryDropdown);
        countrySelect.selectByVisibleText(country);
    }

    public void provideState(String state) {
        waitUntilElementIsClickable(stateInputField);
        stateInputField.clear();
        stateInputField.sendKeys(state);
    }

    public void provideEmail(String email) {
        waitUntilElementIsClickable(emailInputField);
        emailInputField.clear();
        emailInputField.sendKeys(email);
    }

    public void providePassword(String password) {
        waitUntilElementIsClickable(passwordInputField);
        passwordInputField.clear();
        passwordInputField.sendKeys(password);
    }

    public void providePasswordAgain(String repeatPassword) {
        waitUntilElementIsClickable(repeatPasswordInputField);
        repeatPasswordInputField.clear();
        repeatPasswordInputField.sendKeys(repeatPassword);
    }

    public void clickCreateAccount() {
        waitUntilElementIsClickable(createAccountButton);
        createAccountButton.click();
    }

}
