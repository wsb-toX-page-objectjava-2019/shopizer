package wsb;

import org.junit.Test;

import static wsb.Properties.*;

public class RegistrationTest extends Base {

    @Test
    public void registrationTest(){

        PageObject pageObject = new PageObject(driver);
        pageObject.clickMyAccount();
        pageObject.clickRegisterButton();
        RegistrationPage registrationPage = new RegistrationPage(driver);
        registrationPage.provideFirstName(FIRST_NAME);
        registrationPage.provideLastName(LAST_NAME);
        registrationPage.chooseCountry(COUNTRY_POLAND);
        registrationPage.provideState(STATE_NIBYLANDIA);
        registrationPage.provideEmail(EMAIL_ADDRESS);
        registrationPage.providePassword(PASSWORD);
        registrationPage.providePasswordAgain(PASSWORD);
        registrationPage.clickCreateAccount();
        int x = 0;
    }
}