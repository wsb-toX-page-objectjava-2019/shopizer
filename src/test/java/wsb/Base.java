package wsb;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.junit.Assert.assertEquals;

public class Base {

    static final String url = "http://demo.shopizer.com:8080/shop";
    public static WebDriver driver;

    @BeforeClass
    public static void driverSetup() {
        System.setProperty("webdriver.firefox.driver", "geckodriver.exe");
        driver = new FirefoxDriver();
    }

    @Before
    public void setup() {
        driver.get(url);
        String currentUrl = driver.getCurrentUrl();
        assertEquals(url, currentUrl);
        PageObject pageObject = new PageObject(driver);
        pageObject.mainPageLogo.isDisplayed();
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
